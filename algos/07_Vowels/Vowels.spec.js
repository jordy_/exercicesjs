// Given a string of words or phrases, count the number of vowels.
// run tests with : "npm test vowels"

function vowels(input) {
    // insert code here
}


//----------------------
// TESTS
//----------------------

describe("Vowels", () => {
    it("Should count vowels", () => {
        expect(vowels("hello world")).toBe(3)
    })
})
