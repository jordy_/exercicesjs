// For a given number, find all the prime numbers from zero to that number.
// run tests with : "npm test SieveOfEratosthenes"

function primes(input) {
    // insert code here
}


//----------------------
// TESTS
//----------------------

describe("Sieve of Eratosthenes", () => {
    it("Should return all prime numbers", () => {
        expect(primes(10)).toBe([2, 3, 5, 7])
    })
})
