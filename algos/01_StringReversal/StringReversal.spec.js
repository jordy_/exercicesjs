// Given a string of characters as input, write a function that returns it with the characters reversed.
// run tests with : "npm test stringreversal"

function stringReversal(input) {
    // insert code here
}


//----------------------
// TESTS
//----------------------

describe("String Reversal", () => {
    it("Should reverse string", () => {
        expect(stringReversal("Hello World!")).toEqual("!dlroW olleH")
    })
})