// Given a matrix N-M, invert its axis to obtain a matrix M-N
// run tests with : "npm test matrixtilt"

function tilt(input) {
    // insert code here
}


//----------------------
// TESTS
//----------------------

describe("Matrix Tilt", () => {
    it("Should invert matrix axis", () => {
        const matrix = [
            [ 1, 2, 3 ],
            [ "4", "5", "6" ],
            [ 7, 8, 9 ],
            [ "10", "11", "12" ]
        ]
        const expected = [
            [ 1, "4", 7, "10" ],
            [ 2, "5", 8, "11" ],
            [ 3, "6", 9, "12" ]
        ]
        expect(tilt(matrix)).toEqual(expected)
    })
})
