// Anagrams are words or phrases that contain the same number of characters. 
// Create a function that checks for this.
// run tests with : "npm test anagrams"

function anagrams(input) {
    // insert code here
}


//----------------------
// TESTS
//----------------------

describe("Anagrams", () => {
    it("Should implement anagrams", () => {
        expect(max("hello world", "world hello")).toBeTruthy()
        expect(max("hellow world", "hello there")).toBeFalsy()
        expect(max("hellow world", "hello there!")).toBeFalsy()
    })
})
