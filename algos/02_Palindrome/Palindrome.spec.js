// A palindrome is a word or phrase that reads the same backward as forward. Write a function that checks for this.
// run tests with : "npm test palindrome"

function isPalindrome(input) {
    // insert code here
}


//----------------------
// TESTS
//----------------------

describe("Palindrome", () => {
    it("Should return true", () => {
        expect(isPalindrome("Cigar? Toss it in a can. It is so tragic")).toBeTruthy()
    })
    it("Should return false", () => {
        expect(isPalindrome("sit ad est love")).toBeFalsy()
    })
})
