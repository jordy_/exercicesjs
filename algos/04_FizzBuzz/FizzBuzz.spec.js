// Given a number as an input, print out every integer from 1 to that number. 
// However, when the integer is divisible by 2, print out “Fizz”.
// When it’s divisible by 3, print out “Buzz”.
// When it’s divisible by both 2 and 3, print out “Fizz Buzz”.
// run tests with : "npm test fizzBuzz"

function fizzBuzz(input) {
    // insert code here
}


//----------------------
// TESTS
//----------------------
let output

beforeEach(() => {
    output = fizzBuzz(30)
})

describe("Fizz Buzz", () => {
    it("Should output number", () => {
        expect(output[0]).toEqual(1)
    })
    it("Should output Fizz", () => {
        expect(output[1]).toEqual("Fizz")
    })
    it("Should output Buzz", () => {
        expect(output[2]).toEqual("Buzz")
    })
    it("Should output Fizz Buzz", () => {
        expect(output[5]).toEqual("Fizz Buzz")
    })
})
